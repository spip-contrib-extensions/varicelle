<?php


// Utiliser la class .btn dans SPIP :
function varicelle_affichage_final($texte) {
	$texte = str_replace('class="submit', 'class="btn submit', $texte);
	return $texte;
}

function varicelle_insert_head_css($flux) {
	$flux .= '<link rel="stylesheet" type="text/css" href="'.timestamp(find_in_path('css/varicelle.css')).'" media="all" />'."\n";
	return $flux;
}
