<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'varicelle_description' => 'De jolis boutons : séduisants, légers et performants, pour styler tous les boutons du site. S’applique à tout ce qui porte le sélecteur <code>.btn</code> y compris les boutons des formulaires SPIP.',
	'varicelle_slogan' => 'De jolis bouton !',
);
